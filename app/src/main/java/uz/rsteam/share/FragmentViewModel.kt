package uz.rsteam.share

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.debounce


/**
 * Created by rustam on 02/11/2020
 */

class FragmentViewModel: ViewModel() {

    private val flowMutable = MutableStateFlow(1)

    val flow: Flow<Int> = flowMutable.debounce(10 /** 2 * 60*/ * 1000)

    fun trackClickEvent(){
        flowMutable.value = flowMutable.value + 1
    }
}