package uz.rsteam.share

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import androidx.annotation.ColorInt
import androidx.appcompat.R
import androidx.appcompat.widget.Toolbar
import com.google.android.material.circularreveal.CircularRevealHelper
import com.google.android.material.circularreveal.CircularRevealWidget
import com.google.android.material.circularreveal.CircularRevealWidget.RevealInfo


/**
 * Created by rustam on 16/10/2020
 */

class RevealToolbar @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = R.attr.toolbarStyle
) : Toolbar(context, attrs, defStyleAttr), CircularRevealWidget {

    private var helper: CircularRevealHelper = CircularRevealHelper(this)

    override fun buildCircularRevealCache() {
        helper.buildCircularRevealCache()
    }

    override fun destroyCircularRevealCache() {
        helper.destroyCircularRevealCache()
    }

    override fun getRevealInfo(): RevealInfo? {
        return helper.revealInfo
    }

    override fun setRevealInfo(revealInfo: RevealInfo?) {
        helper.revealInfo = revealInfo
    }

    override fun getCircularRevealScrimColor(): Int {
        return helper.circularRevealScrimColor
    }

    override fun setCircularRevealScrimColor(@ColorInt color: Int) {
        helper.circularRevealScrimColor = color
    }

    override fun getCircularRevealOverlayDrawable(): Drawable? {
        return helper.circularRevealOverlayDrawable
    }

    override fun setCircularRevealOverlayDrawable(drawable: Drawable?) {
        helper.circularRevealOverlayDrawable = drawable
    }

    override fun draw(canvas: Canvas) {
        if (helper != null) {
            helper.draw(canvas)
        } else {
            super.draw(canvas)
        }
    }

    override fun actualDraw(canvas: Canvas?) {
        super.draw(canvas)
    }

    override fun isOpaque(): Boolean {
        return if (helper != null) {
            helper.isOpaque
        } else {
            super.isOpaque()
        }
    }

    override fun actualIsOpaque(): Boolean {
        return super.isOpaque()
    }
}