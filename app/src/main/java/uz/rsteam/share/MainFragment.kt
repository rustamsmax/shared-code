package uz.rsteam.share

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.util.TypedValue
import android.view.*
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.circularreveal.CircularRevealCompat
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.flow.collect
import uz.rsteam.share.databinding.FragmentMainBinding


/**
 * Created by rustam on 19/10/2020
 */

class MainFragment: Fragment() {

    private val viewModel: FragmentViewModel by viewModels()

    private val toolbar: RevealToolbar by lazy {
        requireView().findViewById(R.id.toolbar)
    }
    private lateinit var binding: FragmentMainBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as AppCompatActivity).setSupportActionBar(toolbar)
        setHasOptionsMenu(true)

        lifecycleScope.launchWhenStarted {
            viewModel.flow.collect {
                Snackbar.make(requireView(), "View not clicked", Snackbar.LENGTH_LONG).show()
            }
        }

        binding.tvTest.setOnClickListener {
            viewModel.trackClickEvent()
        }
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_list, menu)

        val searchMenu = menu.findItem(R.id.action_search)
        searchMenu.setOnActionExpandListener(
            object : MenuItem.OnActionExpandListener {
                override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                    // Called when SearchView is collapsing
                    if (searchMenu.isActionViewExpanded) {
                        animateSearchToolbar(toolbar, item, false)
                    }
                    return true
                }

                override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                    // Called when SearchView is expanding
                    animateSearchToolbar(toolbar, item, true)
                    return true
                }
            })
//        searchMenu.setActionView(R.layout.layout_search_view)
        val searchView = searchMenu.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
//                viewModel.searchQuery.value = query
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
//                viewModel.searchQuery.value = newText
                return true
            }

        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_scan -> {
//                showScanBarcodeDialog()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }


    fun animateSearchToolbar(
        toolbar: RevealToolbar,
        menuItem: MenuItem,
        show: Boolean
    ) {
        val ctx = toolbar.context
        val rect1 = Rect()
        val rect2 = Rect()
        toolbar.clipChildren = false
        toolbar.getGlobalVisibleRect(rect1)
        toolbar.findViewById<View>(menuItem.itemId).getGlobalVisibleRect(rect2)
        val yPos = toolbar.height / 2f
        if (show) {
            val xPos = rect2.left - rect1.left + rect2.width() / 2f
            val circularReveal = CircularRevealCompat
                .createCircularReveal(toolbar, xPos, yPos, 0.0f, xPos.toFloat())
            circularReveal.duration = 250
            toolbar.setBackgroundColor(ctx.getColorFromThemeAttr(android.R.attr.colorBackground))

            /*val color = ElevationOverlayProvider(ctx)
//                .compositeOverlay(ctx.getColorFromThemeAttr(android.R.attr.colorBackground), ViewCompat.getElevation(toolbar))
                .compositeOverlayWithThemeSurfaceColorIfNeeded(ViewCompat.getElevation(toolbar))
            toolbar.setBackgroundColor(color)*/

            circularReveal.start()
        } else {
            val xPos = rect2.left - rect1.left - rect2.width() / 2f
            val circularReveal = CircularRevealCompat
                .createCircularReveal(toolbar, xPos, yPos, xPos.toFloat(), 0.0f)
            circularReveal.duration = 250
            circularReveal.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    toolbar.clipChildren = true
                    toolbar.setBackgroundColor(ctx.getColorFromThemeAttr(R.attr.colorPrimary))
                }
            })
            circularReveal.start()
        }

    }


    @ColorInt
    fun Context.getColorFromThemeAttr(
        @AttrRes attrColor: Int,
        typedValue: TypedValue = TypedValue(),
        resolveRefs: Boolean = true
    ): Int {
        theme.resolveAttribute(attrColor, typedValue, resolveRefs)
        return typedValue.data
    }
}